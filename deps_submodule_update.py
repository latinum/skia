#!/bin/bash

import ast
import collections
import cStringIO
import tokenize
import subprocess

# partially copied form depot tools
# Just a hacked together script to we can convert those DEPS dependencies to submodules
# Nothing more
# So I don't really care about error handling or handling every case in a DEPS file properly
#
class _NodeDict(collections.MutableMapping):
    """Dict-like type that also stores information on AST nodes and tokens."""

    def __init__(self, data, tokens=None):
        self.data = collections.OrderedDict(data)
        self.tokens = tokens

    def __str__(self):
        return str({k: v[0] for k, v in self.data.iteritems()})

    def __getitem__(self, key):
        return self.data[key][0]

    def __setitem__(self, key, value):
        self.data[key] = (value, None)

    def __delitem__(self, key):
        del self.data[key]

    def __iter__(self):
        return iter(self.data)

    def __len__(self):
        return len(self.data)

    def MoveTokens(self, origin, delta):
        if self.tokens:
            new_tokens = {}
            for pos, token in self.tokens.iteritems():
                if pos[0] >= origin:
                    pos = (pos[0] + delta, pos[1])
                    token = token[:2] + (pos,) + token[3:]
                new_tokens[pos] = token

        for value, node in self.data.values():
            if node.lineno >= origin:
                node.lineno += delta
                if isinstance(value, _NodeDict):
                    value.MoveTokens(origin, delta)

    def GetNode(self, key):
        return self.data[key][1]

    def SetNode(self, key, value, node):
        self.data[key] = (value, node)


def _NodeDictSchema(dict_schema):
    """Validate dict_schema after converting _NodeDict to a regular dict."""
    def validate(d):
        return True
    return validate


def _gclient_eval(node_or_string, vars_dict=None, expand_vars=False,
                  filename='<unknown>'):
    """Safely evaluates a single expression. Returns the result."""
    _allowed_names = {'None': None, 'True': True, 'False': False}
    if isinstance(node_or_string, basestring):
        node_or_string = ast.parse(
            node_or_string, filename=filename, mode='eval')
    if isinstance(node_or_string, ast.Expression):
        node_or_string = node_or_string.body

    def _convert(node):
        if isinstance(node, ast.Str):
            if not expand_vars:
                return node.s
            try:
                return node.s.format(**vars_dict)
            except KeyError as e:
                raise ValueError(
                    '%s was used as a variable, but was not declared in the vars dict '
                    '(file %r, line %s)' % (
                        e.message, filename, getattr(node, 'lineno', '<unknown>')))
        elif isinstance(node, ast.Num):
            return node.n
        elif isinstance(node, ast.Tuple):
            return tuple(map(_convert, node.elts))
        elif isinstance(node, ast.List):
            return list(map(_convert, node.elts))
        elif isinstance(node, ast.Dict):
            return _NodeDict((_convert(k), (_convert(v), v))
                             for k, v in zip(node.keys, node.values))
        elif isinstance(node, ast.Name):
            if node.id not in _allowed_names:
                raise ValueError(
                    'invalid name %r (file %r, line %s)' % (
                        node.id, filename, getattr(node, 'lineno', '<unknown>')))
            return _allowed_names[node.id]
        elif isinstance(node, ast.Call):
            if not isinstance(node.func, ast.Name) or node.func.id != 'Var':
                raise ValueError(
                    'Var is the only allowed function (file %r, line %s)' % (
                        filename, getattr(node, 'lineno', '<unknown>')))
            if node.keywords or node.starargs or node.kwargs or len(node.args) != 1:
                raise ValueError(
                    'Var takes exactly one argument (file %r, line %s)' % (
                        filename, getattr(node, 'lineno', '<unknown>')))
            arg = _convert(node.args[0])
            if not isinstance(arg, basestring):
                raise ValueError(
                    'Var\'s argument must be a variable name (file %r, line %s)' % (
                        filename, getattr(node, 'lineno', '<unknown>')))
            if not expand_vars:
                return '{%s}' % arg
            if vars_dict is None:
                raise ValueError(
                    'vars must be declared before Var can be used (file %r, line %s)'
                    % (filename, getattr(node, 'lineno', '<unknown>')))
            if arg not in vars_dict:
                raise ValueError(
                    '%s was used as a variable, but was not declared in the vars dict '
                    '(file %r, line %s)' % (
                        arg, filename, getattr(node, 'lineno', '<unknown>')))
            return vars_dict[arg]
        elif isinstance(node, ast.BinOp) and isinstance(node.op, ast.Add):
            return _convert(node.left) + _convert(node.right)
        elif isinstance(node, ast.BinOp) and isinstance(node.op, ast.Mod):
            return _convert(node.left) % _convert(node.right)
        else:
            raise ValueError(
                'unexpected AST node: %s %s (file %r, line %s)' % (
                    node, ast.dump(node), filename,
                    getattr(node, 'lineno', '<unknown>')))
    return _convert(node_or_string)


def run_git_command(args):
    proc = subprocess.Popen(['git'] + args, stdout=subprocess.PIPE, stderr=subprocess.PIPE) # call subprocess
    proc.communicate()
    return proc.returncode


def LoadDEPSFile():
        # TODO(alexander): Other base directory support for this
    file_string = open('DEPS').read()
    file_data = ast.parse(file_string, filename='DEPS', mode='exec')
    if isinstance(file_data, ast.Expression):
        file_data = file_data.body

    statements = {}
    for statement in file_data.body:
        statements[statement.targets[0].id] = statement.value

    tokens = {
        token[2]: list(token)
        for token in tokenize.generate_tokens(
            cStringIO.StringIO(file_string).readline)
    }
    local_scope = _NodeDict({}, tokens)

    # Process vars first, so we can expand variables in the rest of the DEPS file.
    # Not strictly required for us, as Skia, at least at the moment, does not use thoses
    vars_dict = {}
    if 'vars' in statements:
        vars_statement = statements['vars']
        value = _gclient_eval(vars_statement, None, False, 'DEPS')
        local_scope.SetNode('vars', value, vars_statement)
        vars_dict.update(value)

    for name, node in statements.iteritems():
        value = _gclient_eval(node, vars_dict, True, 'DEPS')
        local_scope.SetNode(name, value, node)
    return local_scope


def main():
    deps = LoadDEPSFile()['deps']
    for target_dir, remote in deps.iteritems():
        if type(remote) != str:
            continue  
        remote_url = remote.split('@')[0]
        remote_commit = remote.split('@')[1]
        print (target_dir, remote_url)
        # Check if we already have a submodule for this dependency
        if(run_git_command(['submodule', 'status', target_dir]) == 1):
            print("Submodule not found, adding it")
            remote_url = remote.split('@')[0]
            remote_commit = remote.split('@')[1]
            print (remote_url, remote_commit)
            run_git_command(['submodule', 'add', '-f', remote_url, target_dir])
        else:
            print("Submodule already exists, attempting to udpate to correct state")
            run_git_command(['-C', target_dir, 'pull'])
            if (run_git_command(['-C', target_dir, 'checkout', remote_commit]) != 0):
                print('Failed to update to correct state')
            else:
                print('Successfully updated to correct state')


if __name__ == "__main__":
    main()
